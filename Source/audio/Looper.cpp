//
//  Looper.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "Looper.h"

Looper::Looper() :  recordState (false),    //initialise - not playing
                    playState (false),      //not recording
                    bufferPosition (0)      //initialise - start of buffer

{
    
    playState = false;
    recordState = false;
    

    
    audioSampleBuffer.setSize (1, bufferSize);
    audioSampleBuffer.clear();
}

Looper::~Looper()
{

}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState.get();
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState.get();
}

float Looper::processSample (float input)
{
    float output = 0.f;
    float* audioSample = 0;
    if (playState.get() == true)
    {
        //play
        output = *audioSampleBuffer.getReadPointer(0, bufferPosition);
        //click 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 8)) == 0)
            output += 0.25f;
        
        //record
        if (recordState.get() == true)
        {
            audioSample = audioSampleBuffer.getWritePointer(0, bufferPosition);
            *audioSample = input;
        }
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    }
    return output;
}

